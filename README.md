# Leg and Pavement

An optical tremolo, for DIY vactrols, modelled after circuits in Nicolas Collins' 'Handmade Electronic Music'. The very first iterations of this were built to complement Scree Fucking Junk's live strobe stutter. It's been a learning curve for PCB design, detouring through Paper Circuits, to settle in Osmond. It's still not quite there yet but it will start iterating properly from now on.

The name btw is a rather circuitous detour: Scree Fucking Junk are named for a Jack Wormell film > another of his films is called Leg & Pavement > this film is an improvisatory walk whilst zooming in and out > this seemed to encapsulate the in/out of the tremolo.
